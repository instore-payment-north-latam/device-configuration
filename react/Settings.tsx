import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { PageBlock, Input, EXPERIMENTAL_Select, FloatingActionBar, Alert } from 'vtex.styleguide';
import Sellers from './components/Sellers';
import helpers from './commons/helpers/helpers';
import { useGeneralConfig } from './commons/hooks/useConfig';

const Settings = () => {
  const [sellerName, setSellerName] = useState('');
  const [sellerTaxID, setSellerTaxID] = useState('');
  const [taxRate, setTaxRate] = useState('');
  const [storeAccount, setStoreAccount] = useState('');
  const [saveLoader, setSaveLoader] = useState(false);
  const [settingID, setSettingID] = useState('');
  const [showNotify, setShowNotify] = useState(false);
  const [configFound, setConfigFound] = useState(false);
  const baseUrl = helpers.baseUrl;
  const country = helpers.optionsCountries[0];

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const generalConfig = await useGeneralConfig();
    let account = generalConfig.accountName;
    if (account) {
      setStoreAccount(account);
      const query = baseUrl + '/settings/' + account;
      try {
        const settingsRequest = await axios(query);
        const settingsData = settingsRequest.data.data;
        if (settingsData) {
          setSellerName(settingsData.sellerName);
          setSellerTaxID(settingsData.sellerTaxID);
          setTaxRate(settingsData.taxRate);
          setConfigFound(true);
        }

        setSettingID(settingsData._id);
      } catch (error) {
        setConfigFound(false);
        console.error(error.response.data.message);
      }
    }
  };

  const saveData = async () => {
    let settings = {
      seller: storeAccount,
      active: true,
      sellerName: sellerName,
      sellerTaxID: sellerTaxID,
      country: country.value,
      taxRate: taxRate,
      paymentMethods: {
        redeban: {
          active: true,
          url: null,
        },
      },
    };

    let query = baseUrl + '/settings/';
    if (configFound) {
      query += storeAccount;
      await axios.put(query, settings).then(() => {
        setShowNotify(true);
      });
    } else {
      await axios.post(query, settings).then(() => {
        setShowNotify(true);
      });
    }
  };

  return (
    <div className="bg-muted-5 pa8">
      {showNotify && (
        <div className="fixed top-0 right-0">
          <Alert type="success" onClose={() => setShowNotify(false)}>
            Datos guardados con éxito
          </Alert>
        </div>
      )}

      <PageBlock title="Configuración InStore LATAM" subtitle="Configuración general y tiendas." variation="full">
        <div>
          <Input
            dataAttributes={{ 'hj-white-list': true, test: 'string' }}
            label="Nombre"
            value={sellerName}
            onChange={(e: any) => setSellerName(e.target.value)}
          />

          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Identificación tributaria"
                value={sellerTaxID}
                onChange={(e: any) => setSellerTaxID(e.target.value)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Base de impuestos"
                value={taxRate}
                onChange={(e: any) => setTaxRate(e.target.value)}
              />
            </div>
          </div>

          <div className="mt5">
            <EXPERIMENTAL_Select
              disabled={true}
              label="País"
              options={helpers.optionsCountries}
              placeholder="Seleccione país"
              multi={false}
              value={country}
            />
          </div>
          <div className="">
            <h4 className="t-heading-4 mb4 b">Redeban</h4>
            <Input dataAttributes={{ 'hj-white-list': true, test: 'string' }} label="URL WebService SIP" />
          </div>
        </div>
      </PageBlock>

      <PageBlock variation="full">{settingID && <Sellers id={settingID} taxRate={taxRate} />}</PageBlock>

      <div className="pt7 mt7">
        <FloatingActionBar
          save={{
            label: 'Guardar',
            isLoading: saveLoader,
            onClick: () => {
              setSaveLoader(true);
              setTimeout(() => {
                saveData();
                setSaveLoader(false);
              }, 1500);
            },
          }}
        />
      </div>
    </div>
  );
};

export default Settings;
