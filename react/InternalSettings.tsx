import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Input, SelectableCard, Layout, PageHeader, PageBlock, FloatingActionBar, Alert } from 'vtex.styleguide';
import helpers from './commons/helpers/helpers';
import DevicesList from './components/DevicesList';

const InternalSettings = (props: any) => {
  const [sellerName, setSellerName] = useState('');
  const [subtitle, setSubtitle] = useState('Configuración tienda y dispositivos');
  const [selectCard, setSelectCard] = useState('');
  const [message, setMessage] = useState('');
  const [saveLoader, setSaveLoader] = useState(false);
  const [dataForm, setDataForm] = useState(helpers.formInternalSettings);
  const [showNotify, setShowNotify] = useState(false);
  const [showNotifyError, setShowNotifyError] = useState(false);
  const baseUrl = helpers.baseUrl;

  useEffect(() => {
    const fetchData = async () => {
      const query = baseUrl + '/sellers/' + props.params.id;
      await axios(query).then((result) => {
        const data = result.data.data;
        setSelectCard(data.paymentMethods.redeban.model);
        setSellerName(data.sellerName);

        setDataForm({
          ...dataForm,
          country: data.address.country,
          city: data.address.city,
          state: data.address.state,
          latitud: data.address.location.latitude,
          longitude: data.address.location.longitude,
          neighborhood: data.address.neighborhood,
          number: data.address.number,
          pickupPointID: data.pickupPointID,
          seller: data.seller,
          sellerName: data.sellerName,
          complement: data.address.complement,
          terminal: data.paymentMethods.redeban.terminal,
          url: data.paymentMethods.redeban.url,
          _id: data._id,
          model: data.paymentMethods.redeban.model,
        });
        setSubtitle(subtitle + ' -  SellerID: ' + data.seller);
      });
    };
    fetchData();
  }, []);

  const isSelected = (opt: any) => {
    return opt === selectCard;
  };

  let rowsDevices: any[] = [];
  for (var i = 0; i < helpers.redebanDevices.length; i++) {
    const element = helpers.redebanDevices[i];
    rowsDevices.push(
      <div key={element.id} className="w-20">
        <SelectableCard
          hasGroupRigth
          noPadding
          selected={isSelected(element.id)}
          onClick={() => setSelectCard(element.id)}
        >
          <div className="f6 tc pa5">
            <img className="img" src={element.img} alt="" />
            <div>{element.name}</div>
          </div>
        </SelectableCard>
      </div>
    );
  }

  const handleInputChange = (event: any) => {
    setDataForm({
      ...dataForm,
      [event.target.name]: event.target.value,
    });
  };

  const saveData = async () => {
    let seller = {
      address: {
        location: {
          latitude: dataForm.latitud,
          longitude: dataForm.longitude,
        },
        country: dataForm.country,
        state: dataForm.state,
        complement: dataForm.complement,
        neighborhood: dataForm.neighborhood,
        number: dataForm.number,
        postalCode: '',
        street: '',
        city: dataForm.city,
      },
      paymentMethods: {
        redeban: {
          terminal: dataForm.terminal,
          url: dataForm.url,
          model: selectCard,
        },
      },
      sellerName: dataForm.sellerName,
      pickupPointID: dataForm.pickupPointID,
      _configID: props.params.id,
      seller: dataForm.seller,
    };

    let query = baseUrl + '/sellers/' + dataForm._id;
    await axios
      .put(query, seller)
      .then(() => {
        setShowNotify(true);
      })
      .catch((error) => {
        setShowNotifyError(true);
        setMessage(error.response.data.message ? error.response.data.message : 'Verificar datos');
      });
  };

  return (
    <div className="bg-muted-5 pa8">
      {showNotify && (
        <div className="fixed top-0 right-0">
          <Alert type="success" onClose={() => setShowNotify(false)}>
            Datos guardados con éxito
          </Alert>
        </div>
      )}
      {showNotifyError && (
        <div className="fixed top-0 right-0">
          <Alert type="warning" onClose={() => setShowNotifyError(false)}>
            {message}
          </Alert>
        </div>
      )}
      <Layout
        fullWidth
        pageHeader={
          <PageHeader
            linkLabel="REGRESAR A TIENDAS"
            onLinkClick={() => {
              window.location.href = '/admin/app/instore-configuration';
            }}
          ></PageHeader>
        }
      >
        <PageBlock title={sellerName} subtitle={subtitle} variation="aside">
          <div>
            <form>
              <div className="flex mt5">
                <div className="mr2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Nombre de la tienda"
                    value={dataForm?.sellerName}
                    name="sellerName"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Pickup point"
                    value={dataForm?.pickupPointID ? dataForm?.pickupPointID : ''}
                    name="pickupPointID"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </div>

              <div className="flex mt5">
                <div className="mr2 w-50">
                  <Input
                    readOnly={true}
                    dataAttributes={{ 'hj-white-list': true }}
                    label="País"
                    value={dataForm?.country}
                    name="country"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Ciudad"
                    value={dataForm?.city}
                    name="city"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </div>

              <div className="flex mt5">
                <div className="mr2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Estado/Departamento"
                    value={dataForm?.state}
                    name="state"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="mr2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Latitud"
                    value={dataForm?.latitud}
                    name="latitud"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Longitud"
                    value={dataForm?.longitude}
                    name="longitude"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </div>

              <div className="mt5">
                <Input
                  dataAttributes={{ 'hj-white-list': true }}
                  label="Dirección"
                  // value={dataForm?.country + ' - ' + dataForm.city + ' - ' + dataForm.complement }
                  value={dataForm?.address}
                  name="address"
                  onChange={(e: any) => handleInputChange(e)}
                />
              </div>

              <div className="flex mt5">
                <div className="mr2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Número"
                    value={dataForm?.number}
                    name="number"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Barrio"
                    value={dataForm?.neighborhood}
                    name="neighborhood"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>

                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="Complemento"
                    value={dataForm?.complement}
                    name="complement"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </div>

              <div className="flex justify-center mv7">{rowsDevices}</div>

              <div className="flex mt5">
                <div className="mr2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="# Terminal"
                    value={dataForm?.terminal}
                    name="terminal"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
                <div className="ml2 w-50">
                  <Input
                    dataAttributes={{ 'hj-white-list': true }}
                    label="URL WebService SIP"
                    value={dataForm?.url}
                    name="url"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </div>
            </form>
          </div>
          <div>
            <DevicesList properties={props} dataform={dataForm} />
          </div>
        </PageBlock>
      </Layout>

      <div className="pt7 mt7">
        <FloatingActionBar
          save={{
            label: 'Guardar',
            isLoading: saveLoader,
            onClick: () => {
              setSaveLoader(true);
              setTimeout(() => {
                saveData();
                setSaveLoader(false);
              }, 1500);
            },
          }}
        />
      </div>
    </div>
  );
};

export default InternalSettings;
