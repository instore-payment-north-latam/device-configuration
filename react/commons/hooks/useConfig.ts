import { getSearchSeller, getGeneralConfig, getGeneralTest } from '../services/sellers.services';

export const useSearchSeller = (storeID: any, e: any) => {
  const response = getSearchSeller(storeID, e, 1, 1);
  return response;
};

export const useGeneralConfig = () => {
  const response = getGeneralConfig();
  return response;
};

export const useTest = () => {
  const response = getGeneralTest();
  return response;
};
