import axios from 'axios';
import helpers from '../helpers/helpers';

export const getSearchSeller = async (
  storeID: any,
  filterData: any,
  perPage: number,
  numberPage: number
): Promise<any> => {
  let filter = '';
  if (filterData) {
    if (filterData != 'allData') {
      filter = 'name=' + filterData;
    }
  }

  if (perPage) filter = filter + '&per-page=' + perPage;
  if (numberPage) filter = filter + '&number-page=' + numberPage;

  try {
    const query = helpers.baseUrl + '/sellers/list/' + storeID + '?' + filter;
    const response = await axios(query);
    return response.data.data;
  } catch (error) {
    throw new Error(error);
  }
};

export const getGeneralConfig = async (): Promise<any> => {
  const fullResponse = await fetch('/api/vlm/account');
  const responseJson = await fullResponse.json();
  return responseJson;
};

export const getGeneralTest = () => {
  return 'responseJson';
};
