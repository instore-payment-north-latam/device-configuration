import a920 from '../../assets/images/a920.jpg';
import a910 from '../../assets/images/a910.jpg';
import lane from '../../assets/images/lane5000.jpg';
import iwl250 from '../../assets/images/iwl250.jpg';
import move2500 from '../../assets/images/move2500.jpg';
import lane3000 from '../../assets/images/lane3000.jpg';

const helpers = {
  // PROD https://transactions.vtexnorthlatam.com
  // DEV  http://stagingtransactions.vtexnorthlatam.com
  baseUrl: 'https://transactions.vtexnorthlatam.com',
  schemaDevices: {
    properties: {
      deviceLabel: {
        title: 'Dispositivo',
        minWidth: 150,
      },
    },
  },

  schemaSellers: {
    properties: {
      _id: {
        title: 'ID',
        minWidth: 150,
      },
      seller: {
        title: 'Whitelabel',
        minWidth: 200,
      },
      sellerName: {
        title: 'Nombre',
        minWidth: 100,
      },
      devicesCount: {
        title: 'Dispositivos',
        minWidth: 100,
      },
    },
  },

  optionsCountries: [
    {
      value: 'COL',
      label: 'Colombia',
    },
    {
      value: 'PER',
      label: 'Perú',
    },
    {
      value: 'ECU',
      label: 'Ecuador',
    },
  ],

  redebanDevices: [
    {
      name: 'A910',
      id: 'a910',
      img: a910,
    },
    {
      name: 'A920',
      id: 'a920',
      img: a920,
    },
    {
      name: 'Lane5000',
      id: 'lane5000',
      img: lane,
    },
    {
      name: 'Lane3000',
      id: 'lane3000',
      img: lane3000,
    },
    {
      name: 'Move2500',
      id: 'move2500',
      img: move2500,
    },
    {
      name: 'IWL250',
      id: 'iwl250',
      img: iwl250,
    },
  ],

  formInternalSettings: {
    country: '',
    city: '',
    state: '',
    latitud: '',
    longitude: '',
    neighborhood: '',
    number: '',
    pickupPointID: '',
    seller: '',
    sellerName: '',
    complement: '',
    terminal: '',
    model: '',
    url: '',
    _id: '',
    address: '',
    _configID: '',
  },
};

export default helpers;
