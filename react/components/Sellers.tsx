import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Modal, EXPERIMENTAL_Select, Spinner, ModalDialog, Pagination } from 'vtex.styleguide';
// import moment from 'moment'
import helpers from '../commons/helpers/helpers';
import ModalSeller from './ModalSeller';
import { getSearchSeller } from '../commons/services/sellers.services';
import PaginationSellers from './PaginationSellers/PaginationSellers';

const Sellers = (props: any) => {
  const [data, setData] = useState({ hits: [] });
  const [showModal, setShowModal] = useState(false);
  const [showModalConfirm, setShowModalConfirm] = useState(false);
  const [loadSellers, setLoadSellers] = useState(true);
  const [loadData, setLoadData] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loadingTable, setLoadingTable] = useState(false);
  const [sellerID, setSellerID] = useState('');
  const [deleteID, setDeleteID] = useState('');
  const [dataSelect, setDataSelect] = useState([] as any);
  const [searchValue, setSearchValue] = useState('');
  const [totalRows, setTotalRows] = useState(0);
  let filterData = 'allData';
  const baseUrl = helpers.baseUrl;
  let allSellers: any[] = [];
  const storeID = props.id;
  const storeTax = props.taxRate;

  useEffect(() => {
    applyFilter(5, null);
  }, []);

  const sellerData = async () => {
    const query = '/api/catalog_system/pvt/seller/list?SellerType=2';
    await axios(query).then((result) => {
      for (let index = 0; index < result.data.length; index++) {
        const element = result.data[index];
        allSellers.push({
          value: element.SellerId,
          label: element.Name + ' - ID: ' + element.SellerId,
        });
      }
      setDataSelect(allSellers);
      setLoadSellers(false);
    });
  };

  const addStore = () => {
    sellerData();
    setShowModal(true);
  };

  const handleModalToggle = () => {
    setShowModal(false);
    setShowModalConfirm(false);
  };

  const deleteDevices = async () => {
    const query = baseUrl + '/sellers/' + deleteID;
    await axios.delete(query).then(() => {
      location.reload();
    });
  };

  const lineActions = [
    {
      label: (rowData: any) => `Eliminar - ${rowData.rowData.sellerName}`,
      isDangerous: true,
      onClick: (rowData: any) => {
        setDeleteID(rowData.rowData._id);
        setShowModalConfirm(true);
      },
    },
  ];

  const handleConfirmation = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(true);
      deleteDevices();
    }, 1500);
  };

  const handleCancelation = () => {
    setShowModalConfirm(false);
  };

  const handleInputSearchChange = (e: any) => {
    setSearchValue(e.target.value);
  };
  const handleInputSearchClear = () => {
    handleInputSearchSubmit('clear');
  };
  const handleInputSearchSubmit = async (e: any) => {
    const filterValue = e == 'clear' ? 'allData' : searchValue;
    filterData = filterValue;
    applyFilter(5, null);
  };

  const applyFilter = async (numberPerPage: any, currentPage: any) => {
    setLoadingTable(true);
    try {
      const sellers = await getSearchSeller(storeID, filterData, numberPerPage, currentPage);
      setTotalRows(sellers.pagination.total);
      setData(sellers.sellers);
      setLoadingTable(false);
      setLoadData(false);
    } catch (error) {
      console.error(error.response);
      setLoadingTable(false);
    }
  };

  return (
    <div className="table">
      {loadData ? (
        <div className="tc">
          <Spinner />
        </div>
      ) : (
        <Table
          loading={loadingTable}
          fullWidth
          schema={helpers.schemaSellers}
          items={data}
          lineActions={lineActions}
          onRowClick={(rowData: any) => {
            window.location.href =
              '/admin/app/instore-configuration/' + rowData.rowData._id + '?settingsTax=' + storeTax;
          }}
          toolbar={{
            inputSearch: {
              value: searchValue,
              placeholder: 'Buscar...',
              onChange: handleInputSearchChange,
              onSubmit: handleInputSearchSubmit,
              onClear: handleInputSearchClear,
            },
            density: {
              buttonLabel: 'Line density',
              lowOptionLabel: 'Low',
              mediumOptionLabel: 'Medium',
              highOptionLabel: 'High',
            },
            fields: {
              label: 'Toggle visible fields',
              showAllLabel: 'Show All',
              hideAllLabel: 'Hide All',
            },
            newLine: {
              label: 'Agregar tienda',
              handleCallback: () => addStore(),
            },
          }}
        />
      )}

      <PaginationSellers totalRows={totalRows} applyFilter={applyFilter} />

      <Modal centered title="WhiteLabels" isOpen={showModal} onClose={handleModalToggle}>
        <div className="pv7">
          <EXPERIMENTAL_Select
            loading={loadSellers}
            disabled={loadSellers}
            options={dataSelect}
            multi={false}
            menuPosition="fixed"
            onChange={(values: any) => {
              try {
                setSellerID(values.value);
              } catch (error) {
                setSellerID('');
              }
            }}
          />

          {sellerID && <ModalSeller id={sellerID} storeID={storeID} />}
        </div>
      </Modal>

      <ModalDialog
        centered
        loading={loading}
        confirmation={{
          onClick: handleConfirmation,
          label: 'Si, eliminar esta tienda',
          isDangerous: true,
        }}
        cancelation={{
          onClick: handleCancelation,
          label: 'Cancelar',
        }}
        isOpen={showModalConfirm}
        onClose={handleCancelation}
      >
        <div>
          <p className="f3 f3-ns fw3 gray">¿Estás seguro que deseas eliminar esta tienda?</p>
          <p>ID de la tienda: {deleteID}</p>
        </div>
      </ModalDialog>
    </div>
  );
};

export default Sellers;
