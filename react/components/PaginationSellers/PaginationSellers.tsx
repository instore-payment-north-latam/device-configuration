import React, { useState, FunctionComponent } from 'react';
import { Pagination } from 'vtex.styleguide';
import { getSearchSeller } from '../../commons/services/sellers.services';

const rowsOptions = [5, 10, 15, 20, 50];
const initialPagination: any = {
  currentPage: 1,
  currentItemFrom: 1,
  currentItemTo: rowsOptions[0],
  rows: rowsOptions[0],
};

const PaginationSellers: FunctionComponent<any> = ({ totalRows, applyFilter }) => {
  const [page, setPage] = useState(initialPagination);

  const handleNextPage = () => {
    const rows = page.rows;
    const newPage = page.currentPage + 1;
    const itemFrom = page.currentItemTo + 1;
    const itemTo = rows * newPage;
    goToPage(newPage, itemFrom, itemTo, rows);
  };

  const handlePrevPage = () => {
    if (page.currentPage === 0) return;
    const rows = page.rows;
    const newPage = page.currentPage - 1;
    const itemFrom = page.currentItemFrom - rows;
    const itemTo = page.currentItemFrom - 1;
    goToPage(newPage, itemFrom, itemTo, rows);
  };

  const handleRowsChange = (e: any) => {
    let rows = Number(e.target.value);
    const newPage = page.currentPage;
    const itemFrom = page.currentItemFrom;
    const itemTo = itemFrom + rows - 1;
    goToPage(newPage, itemFrom, itemTo, rows);
  };

  const goToPage = async (currentPage: number, currentItemFrom: number, currentItemTo: number, rows: number) => {
    setPage({ currentPage, currentItemFrom, currentItemTo, rows });
    applyFilter(rows, currentPage);
    // const sellers = await getSearchSeller(storeID, 'allData', rows, currentPage);
    // setData(sellers.sellers);
  };

  return (
    <div>
      <Pagination
        rowsOptions={[5, 10, 15, 20, 50]}
        currentItemFrom={page.currentItemFrom}
        currentItemTo={page.currentItemTo}
        onNextClick={handleNextPage}
        onPrevClick={handlePrevPage}
        onRowsChange={handleRowsChange}
        textOf="de"
        textShowRows="Mostrar filas"
        totalItems={totalRows}
      />
    </div>
  );
};

export default PaginationSellers;
