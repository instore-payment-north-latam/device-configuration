import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Modal, Button, Input, InputSearch, ModalDialog } from 'vtex.styleguide';
import helpers from '../commons/helpers/helpers';
import QRCode from 'react-qr-code';
import CryptoJS from 'crypto-js';
import NchanSubscriber from 'nchan';
import moment from 'moment';

const idChannel = '_' + Math.random().toString(36).substr(2, 18);
const DevicesList = (props: any) => {
  const [dataTable, setData] = useState({ hits: [] });
  const [showModal, setShowModal] = useState(false);
  const [addInfo, setAddInfo] = useState(false);
  const [deviceID, setDeviceID] = useState([] as any);
  const [channel, setChannel] = useState([] as any);
  const [showModalConfirm, setShowModalConfirm] = useState(false);
  const [editID, setEditID] = useState('');
  const [loading, setLoading] = useState(false);
  const [infoLabel, setInfoLabel] = useState([] as any);

  const [dataForm, setDataForm] = useState({
    deviceLabel: '',
    deviceModel: '',
    os: '',
    osVersion: '',
    deviceType: '',
  });
  const paramID = props.properties.params.id;
  const dataSeller = props.dataform;
  const storeTax = props.properties.query.settingsTax;
  const baseUrl = helpers.baseUrl;

  useEffect(() => {
    const fetchData = async () => {
      const query = baseUrl + '/devices/list/' + paramID;
      await axios(query)
        .then((result) => {
          const data = result.data.data;
          setData(data);
        })
        .catch((error) => {
          console.error(error.response.data.message);
        });
    };

    fetchData();
  }, []);

  const newLine = (data: any) => {
    if (data) {
      setDeviceID(data.deviceID);
      setAddInfo(true);
      setDataForm({
        ...dataForm,
        deviceLabel: data.deviceLabel,
        deviceModel: data.deviceModel,
        os: data.os,
        osVersion: data.osVersion,
        deviceType: data.deviceType,
      });
    } else {
      createChannel();
      // let DefDeviceID = '_' + Math.random().toString(36).substr(2, 18);
      let DefDeviceID = idChannel;
      setDeviceID(DefDeviceID);
      setAddInfo(false);
      setDataForm({
        deviceLabel: '',
        deviceModel: '',
        os: '',
        osVersion: '',
        deviceType: '',
      });
    }
    setShowModal(true);
  };

  const handleModalToggle = () => {
    setShowModal(false);
  };

  const createChannel = () => {
    // const date = moment().format('MMMM Do YYYY, h:mm:ss a');
    // const encrypted = CryptoJS.SHA256('seller' + date + paramID);
    // const hash = encrypted.toString(CryptoJS.enc.Hex);

    setChannel(idChannel);
    const nchanUrl = 'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + idChannel;
    const sub = new NchanSubscriber(nchanUrl, {
      subscriber: ['websocket', 'longpoll'],
    });

    sub.on('message', function (message: any) {
      const deviceData = JSON.parse(message);
      let device = {
        deviceID: '',
        deviceLabel: '',
        deviceModel: '',
        deviceType: '',
        os: '',
        osVersion: '',
      };

      device.deviceID = deviceData.deviceID;
      device.deviceType = deviceData.deviceType;
      device.deviceModel = deviceData.deviceModel;
      device.os = deviceData.os;
      device.osVersion = deviceData.osVersion;
      device.deviceLabel = deviceData.deviceType + ' ' + deviceData.deviceModel;
      newLine(device);
    });

    sub.on('connect', function (evt: any) {
      console.log('Connect NchanSubscriber', evt);
    });

    sub.start();
  };

  const saveDevice = async () => {
    let dataInfo = {
      _sellerID: paramID,
      deviceID: deviceID,
      deviceLabel: dataForm.deviceLabel,
      deviceName: dataForm.deviceLabel,
      deviceModel: dataForm.deviceModel,
      deviceType: dataForm.deviceType,
      os: dataForm.os,
      osVersion: dataForm.osVersion,
      paymentMethods: {
        redeban: {
          cashierID: '01',
          model: dataSeller.model,
          terminal: dataSeller.terminal,
          url: dataSeller.url,
        },
      },
    };

    const query = baseUrl + '/devices/';
    const urlChannel = 'https://pubsub.vtexnorthlatam.com:9850/pub?id=' + channel;

    if (editID) {
      await axios
        .put(query + editID, dataInfo)
        .then(() => {
          location.reload();
        })
        .catch((error) => {
          console.error('ERROR', error.response.data.message);
          setLoading(false);
        });
    } else {
      try {
        const response = await axios.post(query, dataInfo);
        response.data.data.iva = storeTax;
        await axios.post(urlChannel, response.data.data).then(() => {
          location.reload();
        });
      } catch (error) {
        setLoading(false);
        console.error(error.response.data.message);
      }
    }
  };

  const deleteDevices = async () => {
    const query = baseUrl + '/devices/' + infoLabel._id;
    await axios.delete(query).then(() => {
      location.reload();
    });
  };

  const lineActions = [
    {
      label: (rowData: any) => `Eliminar - ${rowData.rowData.deviceLabel}`,
      isDangerous: true,
      onClick: (rowData: any) => {
        setInfoLabel(rowData.rowData);
        setShowModalConfirm(true);
      },
    },
  ];

  const handleConfirmation = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(true);
      deleteDevices();
    }, 1500);
  };

  const handleCancelation = () => {
    setShowModalConfirm(false);
  };

  const handleInputChange = (event: any) => {
    setDataForm({
      ...dataForm,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="table">
      <div className="flex mt5">
        <div className="mr1">
          <InputSearch
            placeholder="Search..."
            value=""
            size="small"
            onSubmit={(e: any) => {
              e.preventDefault();
              console.log('submitted! search this: ', e.target.value);
            }}
          />
        </div>
        <div className="ml1">
          <Button
            variation="primary"
            size="small"
            onClick={() => {
              newLine('');
            }}
          >
            Agregar
          </Button>
        </div>
      </div>

      <Table
        fullWidth={true}
        schema={helpers.schemaDevices}
        items={dataTable}
        lineActions={lineActions}
        onRowClick={(rowData: any) => {
          const info = rowData.rowData;
          setEditID(rowData.rowData._id);
          newLine(info);
        }}
      />

      <Modal
        centered
        isOpen={showModal}
        onClose={handleModalToggle}
        bottomBar={
          <div className="nowrap">
            <span className="mr4">
              <Button variation="tertiary" onClick={handleModalToggle}>
                Cerrar
              </Button>
            </span>
            <span>
              <Button
                variation="primary"
                isLoading={loading}
                onClick={() => {
                  setLoading(true);
                  saveDevice();
                }}
              >
                Guardar dispositivo
              </Button>
            </span>
          </div>
        }
      >
        <div className="pt4 pb6 tc">
          {!addInfo ? (
            <div>
              <div className="mb6">
                <QRCode value={channel} size={180} />
              </div>
              <Button
                variation="tertiary"
                noUpperCase={true}
                onClick={() => {
                  setAddInfo(true);
                }}
              >
                Agregar dispositivo manualmente
              </Button>
            </div>
          ) : (
            <div>
              <form className="tl">
                <div className="mt5">
                  <Input value={deviceID} readOnly={true} label="ID dispositivo" />
                </div>
                <div className="flex mt5">
                  <div className="mr2 w-50">
                    <Input
                      value={dataForm.deviceLabel}
                      label="Nombre del dispositivo"
                      name="deviceLabel"
                      onChange={(e: any) => handleInputChange(e)}
                    />
                  </div>
                  <div className="ml2 w-50">
                    <Input
                      value={dataForm.deviceModel}
                      label="Modelo"
                      name="deviceModel"
                      onChange={(e: any) => handleInputChange(e)}
                    />
                  </div>
                </div>
                <div className="flex mt5">
                  <div className="mr2 w-50">
                    <Input
                      value={dataForm.os}
                      label="Sistema operativo"
                      name="os"
                      onChange={(e: any) => handleInputChange(e)}
                    />
                  </div>
                  <div className="ml2 w-50">
                    <Input
                      value={dataForm.osVersion}
                      label="Versión de sistema operativo"
                      name="osVersion"
                      onChange={(e: any) => handleInputChange(e)}
                    />
                  </div>
                </div>

                <div className="mt5">
                  <Input
                    value={dataForm.deviceType}
                    label="Tipo"
                    name="deviceType"
                    onChange={(e: any) => handleInputChange(e)}
                  />
                </div>
              </form>
            </div>
          )}
        </div>
      </Modal>

      <ModalDialog
        centered
        loading={loading}
        confirmation={{
          onClick: handleConfirmation,
          label: 'Si, eliminar este dispositivo',
          isDangerous: true,
        }}
        cancelation={{
          onClick: handleCancelation,
          label: 'Cancelar',
        }}
        isOpen={showModalConfirm}
        onClose={handleCancelation}
      >
        <div className="">
          <p className="f3 f3-ns fw3 gray">¿Estás seguro que deseas eliminar este dispositivo?</p>
          <p>Nombre del dispositivo: {infoLabel.deviceLabel}</p>
          <p>ID del dispositivo: {infoLabel._id}</p>
        </div>
      </ModalDialog>
    </div>
  );
};

export default DevicesList;
