import React, { useState } from 'react';
import { EXPERIMENTAL_Select, Box, Input, SelectableCard, Button } from 'vtex.styleguide';
import axios from 'axios';
import helpers from '../commons/helpers/helpers';

const ModalSeller = (props: any) => {
  const [selectCard, setSelectCard] = useState('a920');
  const country = helpers.optionsCountries[0];
  let idData = props.id;
  let storeID = props.storeID;
  const [dataForm, setDataForm] = useState({
    storeName: '',
    pickUpPoint: '',
    country: country.value,
    state: '',
    complement: '',
    neighborhood: '',
    number: '',
    postalCode: '',
    street: '',
    city: '',
    latitud: '',
    longitude: '',
    address: '',
    terminal: '',
    urlWebService: '',
    sellerID: idData,
    _configID: storeID,
    deviceID: '',
    paymentMethods: {
      redeban: {
        active: true,
        url: null,
      },
    },
  });
  const baseUrl = helpers.baseUrl;

  const isSelected = (opt: any) => {
    return opt === selectCard;
  };

  var rows: any[] = [];
  for (var i = 0; i < helpers.redebanDevices.length; i++) {
    const element = helpers.redebanDevices[i];
    rows.push(
      <div key={element.id} className="w-20">
        <SelectableCard
          hasGroupRigth
          noPadding
          selected={isSelected(element.id)}
          onClick={() => {
            setSelectCard(element.id);
          }}
        >
          <div className="f7 tc pa3">
            <img className="img" src={element.img} alt="" />
            <span>{element.name}</span>
          </div>
        </SelectableCard>
      </div>
    );
  }

  const saveStore = async () => {
    let sendData = dataForm;
    sendData['deviceID'] = selectCard;

    let seller = {
      address: {
        location: {
          latitude: dataForm.latitud,
          longitude: dataForm.longitude,
        },
        country: dataForm.country,
        state: dataForm.state,
        complement: dataForm.complement,
        neighborhood: dataForm.neighborhood,
        number: dataForm.number,
        postalCode: '',
        street: '',
        city: dataForm.city,
      },
      paymentMethods: {
        redeban: {
          terminal: dataForm.terminal,
          url: dataForm.urlWebService,
          model: selectCard,
        },
      },
      _configID: dataForm._configID,
      seller: dataForm.sellerID,
      sellerName: dataForm.storeName,
      pickupPointID: dataForm.pickUpPoint,
    };

    const query = baseUrl + '/sellers/';
    await axios
      .post(query, seller)
      .then(() => {
        location.reload();
      })
      .catch((error) => {
        console.error(error.response.data.message);
      });
  };

  const handleInputChange = (event: any) => {
    setDataForm({
      ...dataForm,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="mv5">
      <Box title="Detalles">
        <form>
          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Nombre de la tienda"
                value={dataForm.storeName}
                name="storeName"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Pickup point"
                value={dataForm.pickUpPoint}
                name="pickUpPoint"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
          </div>
          <div className="mt5">
            <EXPERIMENTAL_Select
              label="País"
              options={helpers.optionsCountries}
              placeholder="Seleccione país"
              multi={false}
              disabled={true}
              value={country}
            />
          </div>

          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Estado/Departamento"
                value={dataForm.state}
                name="state"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Ciudad"
                value={dataForm.city}
                name="city"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
          </div>

          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Latitud"
                value={dataForm.latitud}
                name="latitud"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Longitud"
                value={dataForm.longitude}
                name="longitude"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
          </div>

          <div className="mt5">
            <Input
              dataAttributes={{ 'hj-white-list': true, test: 'string' }}
              label="Dirección"
              value={dataForm.address}
              name="address"
              onChange={(e: any) => handleInputChange(e)}
            />
          </div>

          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Número"
                value={dataForm.number}
                name="number"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Barrio"
                value={dataForm.neighborhood}
                name="neighborhood"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>

            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="Complemento"
                value={dataForm.complement}
                name="complement"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
          </div>

          <div className="flex justify-center mv7">{rows}</div>

          <div className="flex mt5">
            <div className="mr2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="# Terminal"
                value={dataForm.terminal}
                name="terminal"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
            <div className="ml2 w-50">
              <Input
                dataAttributes={{ 'hj-white-list': true, test: 'string' }}
                label="URL WebService SIP"
                value={dataForm.urlWebService}
                name="urlWebService"
                onChange={(e: any) => handleInputChange(e)}
              />
            </div>
          </div>
          {/* <input type="submit" value="Submit" /> */}
        </form>
      </Box>

      <div className="mt6 tr">
        <Button
          variation="primary"
          onClick={() => {
            saveStore();
          }}
        >
          Guardar Tienda
        </Button>
      </div>
    </div>
  );
};

export default ModalSeller;
