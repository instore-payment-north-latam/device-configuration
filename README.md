# App VTEX instore device configuration

In this application you can configure the devices that
are going to be registered in the store
for INSTORE transactions

#### Before Installation

Before install this app you should have Instore install in account

- inStore - Getting started and setting up: https://developers.vtex.com/vtex-developer-docs/docs/vtex-io-documentation-vtex-io-cli-install

## Installation

Use VTEX CLI to install

- VTEX CLI https://developers.vtex.com/vtex-developer-docs/docs/vtex-io-documentation-vtex-io-cli-install

## How to run

Remeber tou need use VTEX CLI

```bash
vtex login ACCOUNT_NAME
vtex use WORKSPACE_NAME
vtex link
```

In your account you can view the app with the name LATAM - Instore

## 🔗 Links to more information

[InStore LATAM documentation](https://docs.google.com/document/d/1dTMcpq5p45wtqh7h3rEICeaprooRz3S8AoDePpfMg9w/edit#heading=h.tauuu5rw4h1z)
